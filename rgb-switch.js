const five = require("johnny-five");

five.Board().on("ready", function() {

  // Initialize the RGB LED
  let led = new five.Led.RGB({
    pins: {
      red: 3,
      green: 5,
      blue: 6
    }
  });

  let touch = new five.Button(4);

  const rainbow = ["FF0000", "FF7F00", "FFFF00", "00FF00", "0000FF", "4B0082", "8F00FF"];
  let index = 0;
  let intervalId;

  touch.on("press", () => {
    intervalId && clearInterval(intervalId);

    led.on();

    intervalId = setInterval(() => {
      led.color(rainbow[index++]);
      if (index >= rainbow.length) {
        index = 0;
      }
    }, 200);

  });

  touch.on("release", function() {
    led.off();
    clearInterval(intervalId);
  });

  // Add led to REPL (optional)
  this.repl.inject({
    led, touch
  });

});
