const five = require("johnny-five");

five.Board().on("ready", function() {

  let led = new five.Led(11);

  let rotary = new five.Sensor("A6");

  rotary.scale([0, 255]).on("change", function() {
    led.brightness(this.value);
  });

  led.on();

  this.repl.inject({
    led, rotary
  });

});