const five = require("johnny-five");

five.Board().on("ready", function() {

  let led = new five.Led(11);

  let touch = new five.Button(4);

  touch.on("press", () => {
    led.toggle();
  });

  this.repl.inject({
    led, touch
  });

});
